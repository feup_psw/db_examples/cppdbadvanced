cmake_minimum_required(VERSION 3.16)
project(PostgreSQLExample)

## Compile as C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

## Source and header files
file(GLOB PROGRAM_SOURCES RELATIVE ${PROJECT_SOURCE_DIR}
  "src/*.cpp"
  "src/db/*.cpp"
  "src/logic/*.cpp"
)
file(GLOB PROGRAM_HEADERS RELATIVE ${PROJECT_SOURCE_DIR}
  "include/*.h"
  "include/db/*.h"
  "include/logic/*.h"
)

## System dependencies are found with CMake's conventions
find_package(PostgreSQL REQUIRED)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
set(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories(
  include
  ${PostgreSQL_INCLUDE_DIRS}
)

## Declare a C++ executable
## With catkin_make all packages are built within a single CMake context
## The recommended prefix ensures that target names across packages don't collide
add_executable(${PROJECT_NAME}
  ${PROGRAM_SOURCES}
  ${PROGRAM_HEADERS}
)

## Specify libraries to link a library or executable target against
target_link_libraries(${PROJECT_NAME}
  ${PostgreSQL_LIBRARIES}
)
