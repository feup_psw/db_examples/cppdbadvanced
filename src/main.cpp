#include <iostream>
#include <vector>

#include "db/Connection.h"
#include "db/Query.h"
#include "db/People.h"

#include "logic/People.h"

int main(int argc, char** argv) {
  // Database
  db::Connection dbconn(db::kDbName, db::kDbHostIP, db::kDbUsername,
      db::kDbPassword);
  try {
    dbconn.connect();
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -1;
  }

  // Set static member of Query to not have the Connection obj in all Query objs

  // Set psw schema
  db::Query dbquery(dbconn);
  std::cout << "Setting schema to psw..." << std::endl;
  try {
    dbquery.setSchema("psw");
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -1;
  }

  // Confirm if the schema was changed
  try {
    std::cout << "Search path: " << dbquery.showSearchPath() << std::endl;
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -1;
  }


  /***********************************
   * EXAMPLE QUERIES TO THE DATABASE *
   ***********************************/
  db::People people_query(dbconn);

  // - get all people in the database
  std::vector<logic::People> all_people;
  people_query.getAll(all_people);

  std::cout << std::endl << "All people saved in the database:" << std::endl;
  for (auto single_people : all_people) {
    std::cout << single_people.name  << " "
              << single_people.email << " "
              << single_people.code  << std::endl;
  }

  return 0;
}
