#include "db/People.h"

#include "logic/People.h"

namespace db {

People::People(Connection& dbconn) : Query(dbconn) { }

void People::getAll(std::vector<logic::People>& all) {
  PGresult* result = execute("SELECT * FROM people;");

  for (int i = 0; i < PQntuples(result); i++) {
    all.push_back( logic::People(
        PQgetvalue(result,i,0), PQgetvalue(result,i,1), PQgetvalue(result,i,2))
    );
  }

  PQclear(result);
}

} // namespace db
