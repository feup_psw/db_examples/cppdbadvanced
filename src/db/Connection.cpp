#include "db/Connection.h"

namespace db {

Connection::Connection(const std::string dbname, const std::string host,
                       const std::string user, std::string password)
    : dbname_(dbname) , host_(host) , user_(user) , password_ (password) {
  dbconn_str_ = "dbname=" + dbname_ + " host=" + host_ +
      " user=" + user_ + " password=" + password_ +
      " connect_timeout=2";   // not recommended timeout less than 2s
}

Connection::~Connection() {
  close();
}

void Connection::connect() {
  dbconn_ = PQconnectdb(dbconn_str_.c_str());

  if (PQstatus(dbconn_) != CONNECTION_OK) {
    close();
    
    throw std::runtime_error(
        "[CONNECTION] Error when connecting to the database " + dbconn_str_);
  }
}

void Connection::close() {
  PQfinish(dbconn_);  // even if fail, free allocated memory of PGconn object
  dbconn_ = nullptr;
}

bool Connection::isGood() {
  if (PQstatus(dbconn_) == CONNECTION_OK) {
    return true;
  } else {
    return false;
  }
}

PGconn* Connection::getPGconn() {
  if (isGood()) {
    return dbconn_;
  } else {
    return nullptr;
  }
}

std::string& Connection::getConnectionStr() {
  return dbconn_str_;
}

} // namespace db
