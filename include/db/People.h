#pragma once

#include <vector>

#include "db/Query.h"

// Forward declaration
namespace logic {
  class People;
}

namespace db {

class People : Query {
 public:
  explicit People(Connection& dbconn);

  void getAll(std::vector<logic::People>& all);
};

} // namespace db
