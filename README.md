# PostgreSQL

Configuration of a [PostgreSQL](https://www.postgresql.org/) database and
communication with it through a C++ external application. This configuration is
meant for using CMake as a cross/platform tool for build, test, and package
installation.

## Installation

### IDE / Text Editor

**Visual Studio Code (Ubuntu 20.04.5 LTS)**

See the configuration of a C/C++ development environment in
https://code.visualstudio.com/docs/languages/cpp.

1. Install Visual Studio Code
   1. Go to https://code.visualstudio.com/download
   2. Download `.deb` file
   3. Install VS Code:
      ```sh
      cd Downloads/
      sudo dpkg -i code_1.72.2-1665614327_amd64.deb
      ```
2. Install building tools
   ```sh
   # Update system
   sudo apt update
   sudo apt dist-upgrade

   # Install building and debugging tools
   sudo apt install build-essential gdb
   sudo apt install cmake
   ```
3. Install the extensions CMake and CMake Tools in Visual Studio Code:
   1. Go to Extensions
   2. Install CMake and CMake Tools
   3. Configure the settings of Visual Studio Code
      1. Open Settings (File > Preferences > Settings)
      2. Search `cmake`
      3. Confirm that `Build directory` is `${workspaceFolder}/build`
      4. You can change other options for changing the compilation behavior

**Visual Studio Community 2022 (Windows 11)**

1. Install Visual Studio Community 2022
   1. Go to https://visualstudio.microsoft.com/vs/community/
   2. Download Visual Studio Installer
   3. Add the following workloads for installation:
      - Desktop development with C++
      - Game development with C++
      - Linux and embbeded development with C++
   4. Install Visual Studio Community 2022 while downloading
2. Install CMake
   1. Go to https://cmake.org/download/
   2. Download the `x64` installer of the latest release
   3. Install CMake choosing to add CMake to `Path` in the installation
      procedure

### libpq (PostgreSQL C Client Library)

See the official documentation of the
[libpq](https://www.postgresql.org/docs/current/libpq.html) library for
developing PostgreSQL-based applications.

**Ubuntu 20.04.5 LTS**

1. Open a new terminal
2. Install the libraries and headers for PostgreSQL C development
   ```sh
   # Check for updates
   sudo apt update
   sudo apt dist-upgrade

   # Install package
   sudo apt install libpq-dev postgresql-server-dev-all
   ```

**Windows 11**

1. Go to [PosgreSQL Downloads](https://www.postgresql.org/download/) page
2. Select Windows operating system
3. Download the installer certified by EDB for all supported PostgreSQL versions
   1. Open the webpage
   2. Select latest version of PostgreSQL (v15.0) for Windows x86_64
4. Install PostgreSQL
   - Select all 4 componnents (pgAdmin 4 is not really needed unless you want a
     visualization of the database)
   - No additional software is needed to install through Stack Builder (if you
     were using Java or C#, you would probably want to install `pgJDBC` or
     `psqlODBC` database drivers, respectively)
5. Add PostgreSQL to the `Path` environment variable
   1. Search in Windows for `Edit environment variables`
   2. Choose `Path` variable in your _User variables_ and select
      _Edit_
   3. Select _New_ and add the PostgreSQL binaries folder to path
      (`C:\Program Files\PostgreSQL\15\bin`, make sure this path exists!)
   4. Select _Ok_ to save the updated `Path` variable

### VPN Connection

Source: https://www.up.pt/it/pt/servicos/redes-e-conetividade/vpn-eca13b99.

**Ubuntu 20.04.5 LTS**

1. Open Settings (Show All Applications > Settings)
2. Go to VPN connections (click on Network)
3. Add new VPN connection:
   1. Select Point-to-Point Tunneling Protocol (PPTP)
   2. Fill in the VPN parameters (Identity):
      - Name: `FEUP`
      - Gateway: `feup-vpn.up.pt`
      - User name + Password: your credentials (`...@fe.up.pt`)
        - Click on the symbol next to password > Select _Store the password for
          all users_
      - Advanced > Check _Use Point-to-Point encryption (MPPE)_

**Windows 11**

1. Install Check Point Capsule VPN
   1. Open Microsoft Store
   2. Search for Check Point Capsule VPN
   3. Install the application
2. Open Settings (Right-click Windows logo > Settings)
3. Go to VPN connections (Network & internet > VPN)
4. Add new VPN connection:
   - VPN provider: Check Point Capsule VPN
   - Connection name: `FEUP VPN`
   - Server name or address: `193.136.33.254`
   - User name + Password: your credentials (`...@fe.up.pt`)
   - Check _Remember my sign-in info_

## Usage

### VPN Connection

**Ubuntu 20.04.5 LTS**

1. Open Settings (Show All Applications > Settings)
2. Go to VPN connections (click on Network)
3. Connect `FEUP VPN`

### Database

See [documentation](doc/) for further details on how to create schemas, tables,
and populate the database.

### Compile & Run

**Ubuntu 20.04.5 LTS**

1. Open the project's folder in Visual Studio Code
   ```sh
   # Create a folder relative to the Software Design (PSW) course
   cd
   mkdir -p dev/psw
   cd dev/psw

   # Clone the repository
   git clone https://gitlab.com/feup_psw/db_examples/cppdbadvanced.git
   cd postgresql
   code .
   ```
2. Click `Yes` to setup the cmake and select `g++` compiler
3. Open `CMakeLists.txt` file and Ctrl+S (Output show open automatically and
   generate a `build/` folder in your workspace)
3. Open terminal (Terminal > New Terminal)
   ```sh
   cd build
   make
   ./PostgreSQLExample
   ```

**Windows 11**

1. Open CMake (cmake-gui)
2. Configure the project:
   - Where is the source code: `C:/Users/sousa/Dev/cppdbadvanced`
   - Where to build the binaries: `C:/Users/sousa/Dev/cppdbadvanced/build`
3. Press Configure and Generate (current generator: Visual Studio 17 2022)
4. Click Open Project (will open automatically Visual Studio 2022)
5. Build the project: Right-click on `ALL_BUILD` > Build
6. Run the executable: Right-click on `PostgreSQLExample` > Set as Startup
   Project

## Support

Please contact Ricardo B. Sousa (rbs@fe.up.pt) if you have any questions.

## Authors

- Ricardo B. Sousa ([gitlab](https://gitlab.com/sousarbarb/),
  [github](https://github.com/sousarbarb/),
  [email](mailto:rbs@fe.up.pt))
